module Orbit_Receiver(iclk,  
							 //agc1_d, agc2_d,
							 adc, 
							 adc_ovf,
							 //agc1_cs, agc1_clk, 
							 //agc2_cs, agc2_clk, 
							 phase_ovf, led, 
							 bitstream);

// *** interface ***//
input iclk;
//input agc1_d;
//input agc2_d;
input [11:0]adc;
input adc_ovf;
//output wire agc1_cs;
//output wire agc2_cs;
//output wire agc1_clk;
//output wire agc2_clk;
output wire bitstream;

// *** interconnections *** //
output wire led;					//LED 

//ADC sampling block
reg [1:0] adc_counter;			//adc sample counter
reg signed [11:0]adc_in;		//input data storage, 							sfix12
reg adc_val;						//input data valid

//bandpass filter block
wire signed [11:0]bp_data;		//bpass output wire, 							sfix12
wire bp_val;						//bpass output data valid flag
wire [1:0] bp_err;				//bpass output data error code
reg signed [11:0] bp_out;		//bpass output data,								sfix12

//quadratures block
wire signed [27:0] i_mul;		//bpass * i_car, 									sfix28_En14
wire signed [27:0] q_mul;		//bpass * q_car, 									sfix28_En14
wire signed [11:0] I;			//inphase sample,									sfix12
wire signed [11:0] Q;			//quadrature sample,								sfix12

//quadrature sampling carriere block
wire signed [15:0]i_car;		//I quadrature sampling carriere,			sfix16_En14
wire signed [15:0]q_car;		//Q quadrature sampling carriere,			sfix16_En14
wire car_val;						//syntethized carriere valid flag

//inphase lowpass filter block
wire signed [11:0]ip_data;		//inphase lowpass output wire,				sfix12				
wire ip_val;						//inphase lowpass output data valid flag
wire [1:0] ip_err;				//inphase lowpass output data error code
reg signed [11:0] ip_out;		//inphase output data,							sfix12

//quadrature lowpass filter block
wire signed [11:0]qp_data;		//quadrature lowpass output wire,			sfix12
wire qp_val;						//quadrature lowpass output data valid flag
wire [1:0] qp_err;				//quadrature lowpass output data error code
reg signed [11:0] qp_out;		//quadrature output data,						sfix12

//atan CORDIC block 
wire signed [15:0] phase;		//atan cordic out,								sfix16_En13

//phase normalization block
wire [15:0] phase_norm;			//normalized phase,								sfix16_En13

//lowpass filter block
wire signed [15:0] lp_data;	//lpass output wire,								sfix16_En13
wire lp_val;						//lpass output data valid flag
wire [1:0] lp_err;				//lpass output data error code
reg signed [15:0] lp_out;		//lpqass output data,							sfix16_En13

//phase analysis block
wire signed [31:0] delta_ph;	//NCO step,											sfix32			
output wire phase_ovf;			//phase overflow flag

//output fifo
reg [15:0] bits;					//delayed quadrature sign
reg [15:0] nrst;					//delayed reset

//service wires
wire pll_clk;						//fast PLL output clock for FIR filter 160 MHz
wire pll_lck;						//fast PLL output locked flag
wire reset;							//syncronous reset
wire nreset;						//syncronous reset negativ
//wire adc_val;					//AGC valid
wire [1:0] in_adc_err;			//ADC error code (for Avalon_st compatibility)
// *** assigments *** //
//assign adc_val = !adc_ovf;
assign reset = !nrst[0];
assign nreset = nrst[0];

assign i_mul = (car_val ? (bp_out * i_car) : 28'sh00000000);
assign q_mul = (car_val ? -(bp_out * q_car) : 28'sh00000000);

assign I = i_mul[25:14];
assign Q = q_mul[25:14];

assign phase_ovf = lp_out > 16'sb0010101010101010;	
assign bitstream = bits[0];

assign led = phase_ovf;			//show phase raid

//initialize fast pll module
pll pll_inst(.inclk0(iclk), 
	          .c0(pll_clk),
		       .locked(pll_lck));
					  
//initialize low & high SPI ADC reader module
//ads_reader ads_inst(.clk(clk), 
//						  .ena(reset), 
//						  .adc1_d(agc1_d),
//						  .adc2_d(agc2_d),
//						  .adc1_cs(agc1_cs),
//						  .adc1_clk(agc1_clk),
//						  .adc2_cs(agc2_cs),
//						  .adc2_clk(agc2_clk),
//						  .ads_err(overflow));


//initialize NCO carriage syntethizer
nco nco_inst(.clk(pll_clk),
				 .reset(reset),
             .err(delta_ph),
             .cos(i_car),
             .sin(q_car),
             .val(car_val));

//initialize bandpass filter module
bpass bpass_inst(.clk(pll_clk), 
					  .reset_n(nreset), 
					  .ast_sink_data(adc_in), 
					  .ast_sink_valid(adc_val),
					  .ast_sink_error(in_adc_err),
					  .ast_source_data(bp_data),
					  .ast_source_valid(bp_val),
					  .ast_source_error(bp_err));
					  
//initialize inphase lowpass filter module
qpass ipass_inst(.clk(pll_clk), 
					  .reset_n(nreset), 
					  .ast_sink_data(I), 
					  .ast_sink_valid(adc_val),
					  .ast_sink_error(bp_err),
					  .ast_source_data(ip_data),
					  .ast_source_valid(ip_val),
					  .ast_source_error(ip_err));
					
//initialize quadrature lowpass filter module
qpass qpass_inst(.clk(pll_clk), 
					  .reset_n(nreset), 
					  .ast_sink_data(Q), 
					  .ast_sink_valid(adc_val),
					  .ast_sink_error(bp_err),
					  .ast_source_data(qp_data),
					  .ast_source_valid(qp_val),
					  .ast_source_error(qp_err));

//calculate arctangent by CORDIC algorithm
atan atan2(.areset(reset),
			  .clk(pll_clk),
			  .q(phase),
			  .x(ip_out),
			  .y(qp_out));


//normalize direct phase value to 0 .. pi/2
Phase_Normalizer norm_phase(.clk(pll_clk),
									 .reset(reset),
									 .i_phase(phase),
									 .o_phase(phase_norm));

//initialize lowpass filter module
lpass lpass_inst(.clk(pll_clk), 
					  .reset_n(nreset), 
					  .ast_sink_data(phase_norm), 
					  .ast_sink_valid(adc_val),
					  .ast_sink_error(ip_err | qp_err),
					  .ast_source_data(lp_data),
					  .ast_source_valid(lp_val),
					  .ast_source_error(lp_err));
						
//phase integrator
Phase_Integrator loop(.clk(iclk),
							 .reset(reset),
							 .phase(lp_out),
							 .dPh(delta_ph));
							 
								 
//registers initialization 
initial begin
	adc_counter <= 0;	
	adc_in <= 0;
	adc_val <= 0;
	bp_out <= 0;	
	ip_out <= 0; 
	qp_out <= 0; 
	lp_out <= 0;
	nrst <= 0;
	bits <= 0;
end


//ADC processing
always @(posedge pll_clk or posedge reset) begin
	if (reset == 1'b1) begin
		adc_counter <= 0;
		adc_in <= 0;
		adc_val <= 0;
	end
	else begin
		//save input ADC value in reg to avoid jitter
		adc_counter <= adc_counter + 1;
		if (!adc_counter) begin
			adc_in <= adc;	
			adc_val <= !adc_ovf;
		end
	end
end
				 
//bandpass filter output processing					  
always @(posedge pll_clk or posedge reset) begin
	if (reset == 1'b1) bp_out <= 0;
	else if (bp_val) bp_out <= bp_data; 		//copy valid data
end

//inphase lowpass filter output processing					  
always @(posedge pll_clk or posedge reset) begin
	if (reset == 1'b1) ip_out <= 0; 
	else if (ip_val) ip_out <= ip_data; 		//copy valid data
end

//quadrature lowpass filter output processing					  
always @(posedge pll_clk or posedge reset) begin
	if (reset == 1'b1) qp_out <= 0; 
	else if (qp_val) qp_out <= qp_data; 		//copy valid data
end

//lowpass filter output processing					  
always @(posedge pll_clk or posedge reset) begin
	if (reset == 1'b1) lp_out <= 0;
	else if (lp_val) lp_out <= lp_data; 		//copy valid data
end

//fifo processing
always @(posedge iclk or posedge reset) begin
	if (reset == 1'b1) begin
		bits <= 16'h0000;
	end
	else begin
		bits <= {qp_out[11], bits[15:1]};
	end
end

//delay reset
always @(posedge iclk) begin
	nrst <= {pll_lck, nrst[15:1]};
end

endmodule