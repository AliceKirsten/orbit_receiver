module Phase_Normalizer(clk,
								reset,
								i_phase, 
								o_phase);
			
// *** interface ***//
input clk;											//clock
input reset;										//reset
input signed [15:0] i_phase;					//input signed phase in range [-pi .. pi]
output wire signed [15:0] o_phase;			//output data syncronyzation

//internal regs
reg signed [15:0] i_phase_sync;				//input data syncronyzation
wire signed [15:0] phase_clip1;				//clipped value
wire signed [15:0] phase_clip2;				//clipped value 2
wire signed [15:0] k_norm;
wire [14:0] phase_abs;							//absolute phase
wire signed [15:0] phase_norm;				//signed normalized phase -pi/2...pi/2
wire signed [15:0] ph_norm;					//output normalized phase in range [0 .. pi/2]
reg signed [15:0] phase_clip_reg;			//clipped value 2
reg [14:0] phase_abs_reg;
reg signed [15:0] phase_norm_reg;
reg signed [15:0] ph_norm_reg;

assign phase_clip1 = i_phase_sync + (i_phase_sync >>> 2);
assign phase_clip2 = phase_clip1 + (i_phase_sync >>> 6);
assign k_norm = 16'h3F9E;

//assign phase_abs = (phase_clip2[15] ? -phase_clip2 : phase_clip2);//get the absolute value
//assign phase_norm = k_norm - phase_abs;									//normalize (shifted, pi/2 == 11'h3FFF)
//assign ph_norm = (phase_norm[15] ? -phase_norm : phase_norm);		//get the absolute value

assign phase_abs = (phase_clip_reg[15] ? -phase_clip_reg : phase_clip_reg);//get the absolute value
assign phase_norm = k_norm - phase_abs_reg;											//normalize (shifted, pi/2 == 11'h3FFF)
assign ph_norm = (phase_norm_reg[15] ? -phase_norm_reg : phase_norm_reg);	//get the absolute value

initial begin
	i_phase_sync <= 0;
	phase_clip_reg <= 0;
	phase_abs_reg <= 0;
	phase_norm_reg <= 0;
	ph_norm_reg <= 0;
end

always @(posedge clk or posedge reset) begin
	if (reset == 1'b1) begin
		i_phase_sync <= 0;
		phase_clip_reg <= 0;
		phase_abs_reg <= 0;
		phase_norm_reg <= 0;
		ph_norm_reg <= 0;
	end
	else begin
		i_phase_sync <= i_phase;
		phase_clip_reg <= phase_clip2;
		phase_abs_reg <= phase_abs;	//get the absolute value
		phase_norm_reg <= phase_norm;
		ph_norm_reg <= ph_norm;
	end
end

assign o_phase = ph_norm_reg;

//NOTE:
//max value after CORDIC[11] is 11'd804 == pi => pi/2 = 11'd402 = 11'h192
//max value after CORDIC[16] is 16'd25736 == pi => pi/2 = 11'd12868 = 11'h3244
//max value after clipping is 16'd25736 + (16'd25736/4) + (16'd26736/64) = 16'd32572 ~ 32767
endmodule