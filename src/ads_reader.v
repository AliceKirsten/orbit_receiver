module ads_reader(clk, ena, 
						adc1_d, adc2_d,
						adc1_cs, adc1_clk,
						adc2_cs, adc2_clk,
						ads_err);

//interface
input clk;
input ena;
input adc1_d;
input adc2_d;
output wire adc1_cs;
output wire adc2_cs;
output wire adc1_clk;
output wire adc2_clk;
output reg  ads_err;

//interconnections
wire adc1_r;
wire adc2_r;
wire ads_ready;

reg [7:0]adc1_res;
reg [7:0]adc2_res;

assign ads_ready = adc1_r && adc2_r;

//initialize adc low voltage module
adcxx1sxx1 adc_low(.i_clk(clk),
						 .i_en_h(ena),
						 .i_spi_miso(adc1_d),
						 .o_spi_cs_l(adc1_cs),
						 .o_spi_clk(adc1_clk),
						 .o_ready_h(adc1_r),
						 .o_adc_data(adc1_res));
//initialize adc high voltage module
adcxx1sxx1 adc_high(.i_clk(clk),
						 .i_en_h(ena),
						 .i_spi_miso(adc2_d),
						 .o_spi_cs_l(adc2_cs),
						 .o_spi_clk(adc2_clk),
						 .o_ready_h(adc2_r),
						 .o_adc_data(adc2_res));
			
always @(posedge clk)
begin
    if (ena) begin
		if (!ads_ready) ads_err <= 0;
		else ads_err <= ((adc1_res > 250) && (adc2_res > 250));
	 end
end 
			endmodule