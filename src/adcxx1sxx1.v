// module avtomat read data from ADC101S021 ili ADCxx1Sxx1
// DATA_WITH = 8 or 10

//`define DEBUG

module adcxx1sxx1 
#(
	 parameter DATA_WITH= 8,
	 parameter F_SYS_HZ = 40_000_000,
    parameter F_ADC_HZ = 1_000_000          //  F_adc_MIN  <= (i_clk(F_SYS_HZ) / F_ADC_HZ = ADC_SPEED) <= F_adc_MAX
)

(
    input wire i_clk,                       // input global clk
	 input wire i_en_h,                      // enable this module
	 input wire i_spi_miso,                  // SPI input MISO
	 output wire o_spi_cs_l,                 // SPI CS active LOW
	 output wire o_spi_clk,                  // SPI CLK
	 output wire o_ready_h,                  // adc read data -> ready = 1
	 output wire [DATA_WITH-1:0] o_adc_data  // adc data

`ifdef DEBUG
    ,
    output wire [7:0] d_spi_st,
	 output wire d_tic
`endif
);

localparam ADC_SPEED = F_SYS_HZ / (2 * F_ADC_HZ);  //  F_adc_MIN  <= (i_clk / n = ADC_SPEED) <= F_adc_MAX

reg spi_clk = 1;
reg spi_cs_l = 1;
reg ready_h = 0;                  // strob gotovnost (spi read OK)
reg [DATA_WITH-1:0] adc_data = 0; // output data (read from spi)

reg tic = 0;                      // strob 1 tic algoritm
reg [5:0] count = 6'd0;           // counter dla (tic)
reg [2:0] st = 0;                 // state avtomat wait i_en_h
reg en_count = 0;                 // global enable for read spi
reg [5:0] spi_st = 0;             // state avtomat read spi
reg [15:0] shift_d_r = 0;         // shift data register
reg spi_stop = 0;                 // strob spi read ok

//-----------------------------------------------------------------------------
// avtomat sostoyaniy ADC
//-----------------------------------------------------------------------------
always @(posedge i_clk)
begin
    case (st)
	 
    0:
	 begin
	     if (i_en_h) begin
		      en_count <= 1;
				st <= st + 1'b1;
		  end
		  ready_h <= 0;
	 end
	 
	 1:
	 begin
	     if (spi_stop == 0) begin
		      st <= st + 1'b1;
        end
	 end

	 2:
	 begin
	     if (spi_stop) begin
		      ready_h <= 1;
				en_count <= 0;
				st <= 0;
        end		  
	 end
	 
    endcase
end

//-----------------------------------------------------------------------------
// TIC counter, strob
//-----------------------------------------------------------------------------
always @(posedge i_clk)
begin
    if (en_count) begin
        if (count == ADC_SPEED) begin
	         tic <= 1;
				count <= 0;
	     end else begin
	         tic <= 0;
				count <= count + 1'b1;
	     end
	 end else begin
	     count <= 0;
		  tic <= 0;
	 end
end

//-----------------------------------------------------------------------------
// avtomat spi read
//-----------------------------------------------------------------------------
always @(posedge i_clk)
begin
    if (tic) begin
	     case (spi_st)
		  0: // CS = 1
		  begin
				spi_st <= spi_st + 1'b1;
				spi_stop = 0;
		  end

		  1: // CS = 0 (1->0)
		  begin
		      spi_cs_l <= 0;
				spi_st <= spi_st + 1'b1;
		  end
		  
		  2,4,6,8,10,12,14,16,18,20,22,24,26,28,30: // clk (1->0)
		  begin
		      spi_clk <= 0;
				spi_st <= spi_st + 1'b1;
		  end

		  3,5,7,9,11,13,15,17,19,21,23,25,27,29,31:
		  begin
		      spi_clk <= 1;
				spi_st <= spi_st + 1'b1;
				shift_d_r <= { shift_d_r[14:0], i_spi_miso };
		  end

		  32:
        begin
		      spi_clk <= 0;
				spi_st <= spi_st + 1'b1;
		  end
		  
		  33:
		  begin
		      spi_clk <= 1;
				spi_st <= spi_st + 1'b1;
				spi_cs_l <= 1;
		  end
		  
		  34,35,36,37,38,39,40:
		  begin
		      spi_st <= spi_st + 1'b1;
		  end
		  
		  41:
		  begin
		      spi_st <= 0;
				spi_stop = 1;
				adc_data[DATA_WITH-1:0] <= shift_d_r[11:12-DATA_WITH];
		  end
		      
	     endcase
    end
end



assign o_spi_clk = spi_clk;
assign o_spi_cs_l = spi_cs_l;
assign o_ready_h = ready_h;
assign o_adc_data = adc_data;

`ifdef DEBUG
assign d_spi_st = spi_st;
assign d_tic = tic;
`endif

endmodule