set_time_format -unit ns
derive_clock_uncertainty

derive_pll_clocks

create_clock -name in_clk -period 40MHz [get_ports iclk]

set_output_delay -clock [get_ports iclk] -max 3.0 [get_ports {bitstream phase_ovf}]
set_output_delay -clock [get_ports iclk] -min 4.0 [get_ports {bitstream phase_ovf}]

set_input_delay -clock in_clk -max 1.0 [get_ports {adc[*] adc_ovf}]
set_input_delay -clock in_clk -min 1.0 [get_ports {adc[*] adc_ovf}]

#set_max_delay -from [get_registers loop|sum_int*] -to [get_registers loop|integrator*] 1.0
#set_min_delay -from [get_registers loop|sum_int*] -to [get_registers loop|integrator*] 2.0
 

